void main() {
  print(rangeWithStep(1, 10, 2));
  print(rangeWithStep(11, 23, 3));
  print(rangeWithStep(5, 2, 1));
}

rangeWithStep(var startNum, var finishNum, var step) {
  List<int> listangka = [];
  if (startNum > finishNum) {
    for (var i = finishNum; i <= startNum; i+=step) {
      listangka.add(i);
      listangka.sort((b,a) => a.compareTo(b));
    }
  } else {
    for (var i = startNum; i <= finishNum; i+=step) {
      listangka.add(i);
    }
  }
  return listangka;
}
