import 'dart:io';
import 'dart:math';

var input = [
  ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
  ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
  ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
  ["0004", "Bintang Sejaya", "Martapura", "6/4/1970", "Berkebun"],
];

var array = ['Nomor ID', 'Nama Lengkap', 'TTL', 'Hobby'];

void main() {
  for (var i = 0; i < input.length; i++) {
    print('');
    for (var j = 0; j < input[i].length; j++) {
      if (j == 2) {
        stdout.write("${array[j]} = ${input[i][j]} ");
        print(input[i][j + 1]);
      } else if (j == 3) {
        stdout.write("${array[j]} = ");
        j++;
        print(input[i][j]);
      } else {
        stdout.write("${array[j]} = ${input[i][j]} ");
        print('');
      }
    }
  }
}
