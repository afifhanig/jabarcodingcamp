import 'package:flutter/material.dart';
import 'package:jccappflutter/Tugas/Tugas15/home_page.dart';
import 'package:jccappflutter/Tugas/Tugas15/login_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue
      ),
      home: loginPage(),
    );
  }
}


class navBar extends StatefulWidget{
  int _currentIndex = 0;

  @override
  _navBarState createState() => _navBarState();
}

class _navBarState extends State<navBar>{
  @override
  Widget build (BuildContext context){
    return Scaffold(
     bottomNavigationBar: BottomNavigationBar(
        //onTap: onTappedBar,
        //currentIndex: _currentIndex,  
        items: [
        BottomNavigationBarItem(
          icon: new Icon(Icons.home),
          title: new Text('Home'),
        ),
        BottomNavigationBarItem(
          icon: new Icon(Icons.search),
          title: new Text('Search'),        
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.person), 
          title: Text('Profile'))
        ],
        onTap: (index){
          setState(() {
            //_currentIndex = index;
          });
        },
      ),
    );
  }
}