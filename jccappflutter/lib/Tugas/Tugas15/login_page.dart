import 'package:flutter/material.dart';
import 'package:jccappflutter/Tugas/Tugas15/landing_page.dart';
import 'package:jccappflutter/Tugas/Tugas15/home_page.dart';

class loginPage extends StatefulWidget{
  @override
  _loginPageState createState() => _loginPageState();
}

class _loginPageState extends State<loginPage>{ 
  @override
  Widget build (BuildContext context){
    return Scaffold(
      //backgroundColor: Colors.blueAccent,
      body: SingleChildScrollView(
        child: Container(
        //color: Colors.white,
        margin: EdgeInsets.all(15.0),
        child: Column(
          children: <Widget>[
            Container(
              //color: Colors.black,
              height: 50,
            ),
            Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("Sanber Flutter",
                  style: TextStyle(
                    fontSize: 50, 
                    color: Colors.blue[300],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(10),
                    child: FlutterLogo(
                      size: 150,
                    ),
                  )
                ],
              ),
              //color: Colors.blueGrey,
              height: 250,
            ),
            Container(
              height: 300,
              //color: Colors.deepPurple,
              margin: EdgeInsets.only(left: 10, right: 10),
              padding: EdgeInsets.only(top: 10),
              child: Column(
                children: [
                  TextField(
                    decoration: InputDecoration(
                      hintText: "Username",
                      contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.green, width: 2.0),
                      ),
                       enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.blue, width: 2.0),
                      )
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  TextField(
                    decoration: InputDecoration(
                      hintText: "Password",
                      contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.green, width: 2.0),
                      ),
                       enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.blue, width: 2.0),
                      )
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),     
                  TextButton(
                    onPressed: (){}, 
                    child: Text("Forgot Password?",style: TextStyle(fontSize: 20),
                      )
                    ),
                  SizedBox(
                    height: 10,
                  ),
                  ElevatedButton(
                    onPressed: () {
                      Navigator.push(context, 
                      MaterialPageRoute(builder: (context) => NavBar()));
                    },
                    child: Text('Login'),
                    style: ElevatedButton.styleFrom(
                      fixedSize: Size(340, 40), 
                      primary: Colors.blue[300]
                    ),
                  ),
                   SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("Doesn't Have an Account?", style: TextStyle(fontSize: 16),),
                      TextButton(
                        onPressed: (){}, 
                        child: Text("Sign In",style: TextStyle(fontSize: 16),
                        )
                      )
                    ],
                  )
                ],
              ),
            ),
            Container(
              //color: Colors.lightGreen,
              padding: EdgeInsets.only(left: 15, right: 15),
              height: 125,
              child: GridView.count(
                crossAxisCount: 2,
                childAspectRatio: 16/9,
                crossAxisSpacing: 20,
                children: [
                  Container(
                    decoration: BoxDecoration(
                    color: Colors.blue[300],
                    image: const DecorationImage(
                      image: AssetImage("assets/images/Paris.png"), scale: 0.8),
                    borderRadius:
                      BorderRadius.all(Radius.circular(20))),
                  ),
                  Container(
                    decoration: BoxDecoration(
                    color: Colors.blue[300],
                    image: const DecorationImage(
                      image: AssetImage("assets/images/London.png"), scale: 0.8),
                    borderRadius:
                      BorderRadius.all(Radius.circular(20))),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    )
  );
}
}