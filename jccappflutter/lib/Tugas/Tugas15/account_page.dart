import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:jccappflutter/Tugas/Tugas15/home_page.dart';
import 'package:jccappflutter/Tugas/Tugas15/search_page.dart';

class accountPage extends StatefulWidget {
  @override
  _accountPageState createState() => _accountPageState();
}

class _accountPageState extends State<accountPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Account Page"),
        backgroundColor: Colors.amber[300],     
      ),
      body: Center(
        child: RichText(
          text: TextSpan(
          children: <TextSpan>[
            TextSpan(text: "Account", style: TextStyle(fontSize: 50, color: Colors.blue[300])),
            TextSpan(text: " Page", style: TextStyle(fontSize: 50, color: Colors.blue[900]))
            ]
          )
        )
      ),
    );
  }
}
