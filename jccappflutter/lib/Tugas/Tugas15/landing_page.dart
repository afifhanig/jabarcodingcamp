import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jccappflutter/Tugas/Tugas15/account_page.dart';
import 'package:jccappflutter/Tugas/Tugas15/search_page.dart';
import 'package:jccappflutter/Tugas/Tugas15/home_page.dart';


class NavBar extends StatefulWidget{
  @override
  _navbarState createState() => _navbarState();
}

class _navbarState extends State<NavBar>{
  int _currentIndex = 0;
  List <Widget> _container = [
    Homepage(),
    searchPage(),
    accountPage(),
  ];

  @override
  Widget build(BuildContext context){
    return Scaffold(
      body: _container[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        //onTap: onTappedBar,
        selectedItemColor: Colors.amber[300],
        currentIndex: _currentIndex,  
        items: [
        BottomNavigationBarItem(
          icon: new Icon(Icons.home),
          title: new Text('Home'),
        ),
        BottomNavigationBarItem(
          icon: new Icon(Icons.search),
          title: new Text('Search'),        
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.person), 
          title: Text('Profile'))
        ],
        onTap: (index){
          setState(() {
            _currentIndex = index;
          });
        },
      ),
    );
  }
}