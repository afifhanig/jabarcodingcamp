import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class searchPage extends StatefulWidget{
  @override
  _searchPageState createState() => _searchPageState();
}

class _searchPageState extends State<searchPage>{ 
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text("Search Page"),
        backgroundColor: Colors.amber[300],
      ),
      body: Center(
        child: RichText(
          text: TextSpan(
          children: <TextSpan>[
            TextSpan(text: "Search", style: TextStyle(fontSize: 50, color: Colors.blue[300])),
            TextSpan(text: " Page", style: TextStyle(fontSize: 50, color: Colors.blue[900]))
            ]
          )
        )
      )
    );
  }
}