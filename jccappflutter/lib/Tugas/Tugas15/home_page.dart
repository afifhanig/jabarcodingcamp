import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:jccappflutter/Tugas/Tugas15/login_page.dart';
import 'package:jccappflutter/Tugas/Tugas15/account_page.dart';
import 'package:jccappflutter/Tugas/Tugas15/search_page.dart';
import 'package:jccappflutter/Tugas/Tugas15/a_drawwer.dart';

class Homepage extends StatefulWidget {
  @override
  _HomepageState createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          Padding(padding: const EdgeInsets.all(8))
        ],
        title: Text("Home Page"),
        backgroundColor: Colors.amber[300],
        systemOverlayStyle: SystemUiOverlayStyle(statusBarColor: Colors.blue[300]),
      ),
      drawer: DrawerScreen() ,
      //backgroundColor: Colors.blueAccent,
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 0),
          //color: Colors.white,
          child: Column(
            children: [
              Container(
                //color: Colors.amber,
                height: 50,
                margin: EdgeInsets.only(bottom: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Container(
                        margin: EdgeInsets.only(right: 10),
                        child: Image(
                            image:
                                AssetImage("assets/icon/notifications.png"))),
                    Container(
                        child: Image(
                            image: AssetImage(
                                "assets/icon/add_shopping_cart.png")))
                  ],
                ),
              ),
              Container(
                //color: Colors.blueGrey,
                height: 140,
                margin: EdgeInsets.only(bottom: 10, right: 10, left: 10),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text.rich(
                    TextSpan(children: <TextSpan>[
                      TextSpan(
                          text: "Welcome, \n",
                          style: TextStyle(color: Colors.blue[300])),
                      TextSpan(
                          text: "Afif Hani",
                          style: TextStyle(color: Colors.blue[900]))
                    ]),
                    style: TextStyle(fontSize: 45),
                  ),
                ),
              ),
              Container(
                height: 100,
                margin: EdgeInsets.only(bottom: 10, right: 10, left: 10),
                child: TextField(
                  decoration: InputDecoration(
                      hintText: 'Search',
                      contentPadding:
                          EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                      prefixIcon: Icon(Icons.search),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.green, width: 2.0),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.blue, width: 2.0),
                      )
                      /*border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(32.0)),
                          //fillColor: Colors.blue,*/
                      ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 10),
                alignment: Alignment.centerLeft,
                child: Text(
                  "Recommendation Place",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
              ),
              Container(
                //color: Colors.lightGreen,
                height: 240,
                alignment: Alignment.topLeft,
                child: GridView.count(
                  crossAxisCount: 2,
                  childAspectRatio: 1.5,
                  mainAxisSpacing: 5,
                  crossAxisSpacing: 20,
                  padding: EdgeInsets.all(5),
                  children: <Widget>[
                    Container(
                      //padding: const EdgeInsets.all(8),
                      decoration: BoxDecoration(
                          //color: Colors.teal[100],
                          image: const DecorationImage(
                              image: AssetImage("assets/images/Monas.png")),
                          borderRadius: BorderRadius.all(Radius.circular(20))),
                    ),
                    Container(
                      //padding: const EdgeInsets.all(8),
                      decoration: BoxDecoration(
                          //color: Colors.teal[200],
                          image: const DecorationImage(
                              image: AssetImage("assets/images/Roma.png")),
                          borderRadius: BorderRadius.all(Radius.circular(20))),
                    ),
                    Container(
                      //padding: const EdgeInsets.all(8),
                      decoration: BoxDecoration(
                          //color: Colors.teal[300],
                          image: const DecorationImage(
                              image: AssetImage("assets/images/Berlin.png")),
                          borderRadius: BorderRadius.all(Radius.circular(20))),
                    ),
                    Container(
                      //padding: const EdgeInsets.all(8),
                      decoration: BoxDecoration(
                          //color: Colors.teal[400],
                          image: const DecorationImage(
                              image: AssetImage("assets/images/Tokyo.png")),
                          borderRadius: BorderRadius.all(Radius.circular(20))),
                    ),
                  ],
                ),
              ),  
            ],
          )
        ),
      )
      
    );
  }
}

