import 'dart:io';

void main() {
  soalno1();
  print("");
  soalno2();
  print("");
  soalno3();
  print("");
  soalno4();
  print("");
}

void soalno1() {
  var jum = 2;
  print("Soal No. 1");
  print("Looping Pertama");
  while (jum <= 20) {
    print(jum.toString() + " - I love Coding");
    jum += 2;
  }
  var jum2 = 20;
  print("");
  print("Looping Kedua");
  while (jum2 >= 2) {
    print(jum2.toString() + " - I Will Become a Mobile Developer");
    jum2 -= 2;
  }
}

void soalno2() {
  print("Soal No. 2");
  for (var angka = 1; angka <= 20; angka++) {
    if (angka % 2 != 0) {
      print(angka.toString() + " - Santai");
    } else if (angka % 2 == 0) {
      print(angka.toString() + " - Berkualitas");
    }

    if (angka % 3 == 0 && angka % 2 != 0) {
      print(angka.toString() + " - I Love Coding");
    }
  }
}

void soalno3() {
  print("Soal No. 3");
  for (int i = 0; i < 4; i++) {
    for (int j = 0; j < 8; j++) {
      stdout.write("#");
    }
    print("");
  }
}

void soalno4(){
  print("Soal No. 4");
  for (int i = 0; i < 7; i++) {    
    stdout.write("#");
    
    for(int j = 0; j < i; j++){
      stdout.write("#");
    }
    print("");
  }
}
