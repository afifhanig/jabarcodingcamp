import 'armor_titan.dart';
import 'attack_titan.dart';
import 'beast_titan.dart';

void main(List<String> args) {
  armorTitan titanArmor = new armorTitan();
  attackTitan titanAttack = new attackTitan();
  beastTitan titanBeast = new beastTitan();

  titanArmor.PowerPoint = 4;
  titanAttack.PowerPoint = 5;
  titanBeast.PowerPoint = 6;
  
  print("");  
  print("Levelpoint dari Titan Armor :  ${titanArmor.PowerPoint} ");
  print("Sifat dari Titan Armor : " + titanArmor.terjang());
  print("");
  print("Levelpoint dari Titan Attack :  ${titanAttack.PowerPoint} ");
  print("Sifat dari Titan Attack : " + titanAttack.punch());
  print("");
  print("Levelpoint dari Titan Beast :  ${titanBeast.PowerPoint} ");
  print("Sifat dari Titan Beast : " + titanBeast.lempar());
  print("");
}
