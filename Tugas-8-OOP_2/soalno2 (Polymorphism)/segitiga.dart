import 'bangun_datar.dart';

class segitiga extends bangunDatar{
  double a = 0.0;
  double b = 0.0;
  double c = 0.0;
  
  segitiga(double a, double b, double c){
    this.a = a;
    this.b = b;
    this.c = c;
  }

  @override
  Map convert(){
    return{
      'luas' : 0.5*a*b, 'keliling' : a+b+c
    };
  }
}