import 'bangun_datar.dart';

class lingkaran extends bangunDatar{
  double jarijari = 0.0;
  static const phi = 3.14;

  lingkaran (double jarijari){
    this.jarijari = jarijari;
  }

  @override
  Map convert(){
    return{
      'luas' : phi*jarijari*jarijari, 'keliling' : 2*phi*jarijari
    };
  }

  
  
}