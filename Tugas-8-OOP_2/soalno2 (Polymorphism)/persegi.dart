import 'bangun_datar.dart';

class persegi extends bangunDatar{
  double sisi = 0.0;

  persegi(double sisi){
    this.sisi = sisi;
  }

  @override
  Map convert(){
    return{
      'luas' : sisi*sisi, 'keliling' : 4*sisi
    };
  }

}