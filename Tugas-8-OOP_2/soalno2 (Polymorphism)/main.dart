import 'bangun_datar.dart';
import 'lingkaran.dart';
import 'persegi.dart';
import 'segitiga.dart';

void main(List<String> args) {
  bangunDatar bangunD = new bangunDatar();
  lingkaran ling = new lingkaran(4);
  persegi kotak = new persegi(5);
  segitiga s3 = new segitiga(6, 7, 8);

  //tugasbangunD.convert();

  print("Luas lingkaran : ${ling.convert()['luas']}");
  print("Keliling lingkaran : ${ling.convert()['keliling']}");
  print("");
  print("Luas persegi : ${kotak.convert()['luas']}");
  print("Keliling persegi : ${kotak.convert()['keliling']}");
  print("");
  print("Luas segitiga : ${s3.convert()['luas']}");
  print("Keliling segitiga : ${s3.convert()['keliling']}");

}