class Segitiga {
  double alas = 0.0;
  double tinggi = 0.0;

  double hitungluas() {
    return this.alas * tinggi * 0.5;
  }
}

void main() {
  Segitiga s1;
  double luasSegitiga;

  s1 = new Segitiga();
  s1.alas = 2.0;
  s1.tinggi = 3.0;
  luasSegitiga = s1.hitungluas();
  print(luasSegitiga);
}
