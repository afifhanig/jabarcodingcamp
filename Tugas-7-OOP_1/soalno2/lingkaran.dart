class Lingkaran{
  double _jarijari = 0.0;
  double _phi = 3.14;

  void setJarijari(double nilai){
    if(nilai < 0){
      nilai *= -1;
    }
    _jarijari = nilai;
  }

  double getJarijari(){
    return _jarijari;
  }

  double getLuas(){
    return _phi*_jarijari*_jarijari;
  }

}