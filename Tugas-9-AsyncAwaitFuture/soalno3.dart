void main(List<String> args) async {
  print("Ready. Sing");
  print(await line1());
  print(await line2());
  print(await line3());
  print(await line4());
}

Future<String>line1() async{
  String line1  = "Pernahkah kau merasa...";
  return await Future.delayed(Duration(seconds: 3), () => line1);
}

Future<String>line2() async{
  String line2  = "Pernahkah kau merasa..........";
  return await Future.delayed(Duration(seconds: 3), () => line2);
}

Future<String>line3() async{
  String line3  = "Pernahkah kau merasa";
  return await Future.delayed(Duration(seconds: 3), () => line3);
}

Future<String>line4() async{
  String line4  = "Hatimu hampa, pernahkah kau merasa hatimu kosong..";
  return await Future.delayed(Duration(seconds: 3), () => line4);
}