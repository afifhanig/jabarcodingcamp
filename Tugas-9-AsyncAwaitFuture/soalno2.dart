Future delayedPrint(int second, String message){
  final duration = Duration(seconds: second);
  return Future.delayed(duration).then((value) => message);
}

void main(List<String> args) {
  print("Life");
  delayedPrint(2, "Never Flat").then((status){
    print(status);
  });
  print("is");
}
