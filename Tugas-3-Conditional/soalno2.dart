import 'dart:io';

void main(){
  print("Selamat datang di Warewolf");
  print("Silahkan masukan nama anda : ");
  String nama = stdin.readLineSync()!;
  print("Silahkan masukan peran anda : ");
  String peran = stdin.readLineSync()!;
  
  if(nama.isEmpty){
    print("Nama harus diisi");
  }
  else if(peran.isEmpty){
    print("Halo "+ nama + " silahkan masukan peran anda :");
    peran = stdin.readLineSync()!;  
  }  
  //
  if(peran.isNotEmpty){
    if(peran == 'penyihir'){
      print("Selamat datang di Dunia Werewolf, "+ nama + ". Halo Penyihir " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!");
    }  
    else if(peran == 'guard'){
      print("Selamat datang di Dunia Werewolf, "+ nama + ". Halo Guard " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf.");
    }
    else if(peran == 'warewolf'){
      print("Selamat datang di Dunia Werewolf, "+ nama + ". Halo Warewolf " + nama + ", kamu akan memakan mangsa setiap malam!");
    }
    else{
      print("salah input peran");
    }
  }
}